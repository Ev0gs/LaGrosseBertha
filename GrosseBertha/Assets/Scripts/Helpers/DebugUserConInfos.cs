﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class DebugUserConInfos : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI userNameText = default;
    [SerializeField] private TextMeshProUGUI publicIpText = default;
    [SerializeField] private TextMeshProUGUI authentUniqueIdText = default;
    public static DebugUserConInfos Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }
    public void UpdateInfos(string userName = null, string publicIp = null, string authentUniqueId = null)
    {
        if (userName is not null) userNameText.text = "Pseudo : " +userName;
        if (publicIp is not null) publicIpText.text = "Ip : "+publicIp;
        if (authentUniqueId is not null) authentUniqueIdText.text = "Id : "+authentUniqueId;
    }

}
