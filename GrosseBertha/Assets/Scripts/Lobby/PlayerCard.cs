using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerCard : MonoBehaviour
{
    [SerializeField] private TMP_Text playerName = default;

    public void SetPlayerInfo(string playerNAME)
    {
        playerName.text = playerNAME;
    }
}
