using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerList : MonoBehaviour
{
    public string PlayerName;
    public bool IsHost;

    public Text PlayerNameText;
    public RawImage PlayerIcon;

}
