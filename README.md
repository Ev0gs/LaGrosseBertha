# GrosseBertha (Unity 2023.1)

## Installation
1. Installer la version 2023.1 de Unity (Current Alpha : 2023.1.0a26)
2. Cloner le Repository via GitHub Desktop
3. Ouvrir Unity Hub
4. Cliquer sur Open Project
5. Sélectionner le dossier du projet contenant l'ensemble des données
